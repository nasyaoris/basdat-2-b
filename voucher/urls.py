from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'voucher'
urlpatterns = [
    path('create/', views.create, name='create'),
    path('lists/', views.lists, name='lists'),
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
