# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.db import connection
from django.contrib import messages
import http.client
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from random import randint


def home(request):
    return render(request, 'home.html')

def role(request):
    return render(request, 'pickrole.html')

def signup(request,role):
    if (role == 'anggota'):
        print("role: anggota")
        return render(request, 'signupanggota.html')
    elif (role == 'petugas'):
        print("role: petugas")
        return render(request, 'signuppetugas.html')
    print("admin")
    return render(request, 'signupadmin.html')

@csrf_exempt
def signupAttempt(request, role):
    ktp = request.POST['ktp']
    email = request.POST['email']
    nama = request.POST['nama']
    alamat = request.POST['alamat']
    tgl_lahir = request.POST['tgl_lahir']
    no_telp = request.POST['no_telp']
    no_kartu = generate_no_kartu()
    gaji='30000'
    saldo = '0'
    points = '0'
    cursor = connection.cursor()
    cursor.execute("SELECT * from PERSON where EMAIL='"+email+"'")
    select = cursor.fetchall()
    if (select):  
        return HttpResponse("Email sudah terdaftar")
    if (role == "anggota"):
        value = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tgl_lahir+"','"+no_telp+"'"
        cursor.execute("INSERT INTO PERSON( ktp,email,nama,alamat,tgl_lahir, no_telp) VALUES ("+value+")")
        anggota = "'"+no_kartu+"',"+ saldo + "," + points +"," + "'"+ktp+"'"
        cursor.execute("INSERT INTO Anggota( no_kartu,saldo,points,ktp) VALUES ("+anggota+")")
        messages.success(request, "Register Berhasil: "+ nama)
        return HttpResponseRedirect('/home/login/')
    elif (role == "petugas"):
        value = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tgl_lahir+"','"+no_telp+"'"
        cursor.execute("INSERT INTO PERSON( ktp,email,nama,alamat,tgl_lahir, no_telp) VALUES ("+value+")")
        petugas = "'"+ktp+"',"+ gaji
        cursor.execute("INSERT INTO petugas (ktp,gaji) values (" + petugas + ")")
        messages.success(request, "Register Berhasil: "+ nama)
        return HttpResponseRedirect('/home/login/')
    return HttpResponseRedirect('/home/login/')

    
def login(request):
    if ("role" in request.session):
        return HttpResponseRedirect(request.session["role"].lower()+'/')
    return render(request, 'login.html')

@csrf_exempt
def loginAttempt(request):
    ktp = request.POST['ktp']
    email = request.POST['email']
    cursor=connection.cursor()
    cursor.execute("SELECT * FROM PERSON WHERE ktp ='"+ktp+"';")
    person = cursor.fetchone()
    if(person):
        cursor.execute("SELECT email FROM PERSON WHERE ktp ='"+ktp+"';")
        select = cursor.fetchone()
        if(select):
            cursor.execute("SELECT * from anggota WHERE ktp ='"+ktp+"';")
            anggota = cursor.fetchone()
            if(anggota):
                cursor.execute("SELECT nama FROM PERSON WHERE ktp ='"+ktp+"';")
                data = cursor.fetchone()
                nama = data[0]
                request.session['nama'] = nama
                request.session['email'] = email
                request.session['ktp'] = ktp
                request.session['role'] = 'anggota'
                return HttpResponseRedirect('/bikeshare/daftarSepeda/')
            else:
                cursor.execute("SELECT nama FROM PERSON WHERE ktp ='"+ktp+"';")
                data = cursor.fetchone()
                nama = data[0]
                request.session['nama'] = nama
                request.session['email'] = email
                request.session['ktp'] = ktp
                request.session['role'] = 'petugas'
                return HttpResponseRedirect('/bikeshare/daftarPenugasan/')
        else:
            return HttpResponse("Email tidak terdaftar")
    else:
        return HttpResponse("No ktp tidak terdaftar")           


def logout(request):
    request.session.clear()
    return render(request, 'login.html')


def generate_no_kartu():
    kartu = randint(10000000, 99999999)
    kartu = str(kartu)
    no_kartu=''
    for i in range(0,8):
        no_kartu= no_kartu + kartu[i]

    cursor=connection.cursor()
    cursor.execute("SELECT * from anggota where no_kartu='"+no_kartu+"'")
    hasil=cursor.fetchone()
    if (hasil):
        if len(hasil)>0:
            generate_no_kartu()
    
    else:
        return no_kartu


# Create your views here.
