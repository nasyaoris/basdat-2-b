from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'home'
urlpatterns = [
    path('', views.home, name='home'),
    path('home/role/', views.role, name='role'),
    path('home/signup/<str:role>/', views.signup, name='signup'),
    path('signupAttempt/<str:role>/', views.signupAttempt, name='signupAttempt'),
    path('home/login/', views.login, name='login'),
    path('home/logout/', views.logout, name='logout'),
    path('loginAttempt/', views.loginAttempt, name='loginAttempt')
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
