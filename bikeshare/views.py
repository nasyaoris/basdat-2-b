from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
from django.shortcuts import render


def daftarPenugasan(request):
    response = {}
    cursor = connection.cursor()

    cursor.execute("select p.ktp, a.nama, p.start_datetime, p.end_datetime, s.nama from stasiun as s, penugasan as p, person as a where s.id_stasiun = p.id_stasiun and p.ktp = a.ktp;")
    result = cursor.fetchall()
    response['listPenugasan'] = result

    return render(request, 'daftarPenugasan.html', response)

def formPenugasan(request):
    response={}
    cursor = connection.cursor()

    cursor.execute("select nama from stasiun")
    result = cursor.fetchall()

    response['stasiun'] = result

    cursor.execute("select penugasan.ktp, person.nama from penugasan, person where person.ktp = penugasan.ktp;")
    petugas = cursor.fetchall()
    response['petugas'] = petugas

    return render(request, 'formPenugasan.html', response)

def createPenugasan(request):
    if(request.method == 'POST'):
        nama_petugas = request.POST['nama_petugas']
        startDate = request.POST['startDate']
        endDate = request.POST['endDate']
        stasiun = request.POST['stasiun']

        split= nama_petugas.split('-')
        ktp = split[0][:-1]

        cursor = connection.cursor()
        cursor.execute("select id_stasiun from stasiun where nama ='"+stasiun+"';")
        id_stasiun = cursor.fetchone()
        cursor.execute("INSERT INTO penugasan(ktp,start_datetime,id_stasiun,end_datetime) values("+"'"+str(ktp)+"','" +startDate+"','" +str(id_stasiun[0]) +"','" + endDate + "');")

        messages.error(request, "success")
        return HttpResponseRedirect('/bikeshare/daftarPenugasan')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/bikeshare/formPenugasan')

@csrf_exempt
def delete_daftarPenugasan(request):
    if (request.method == "POST"):
        petugas = request.POST['ktp']
        split= petugas.split('-')
        ktp = split[0]
        cursor = connection.cursor()
        cursor.execute("delete from penugasan where ktp='"+ktp+"';")
        print(cursor.statusmessage)
        messages.error(request, "success")
        return HttpResponseRedirect('/bikeshare/daftarPenugasan')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/bikeshare/daftarPenugasan')

@csrf_exempt
def update_penugasan(request):
    if (request.method == "POST"):
        nama_petugas = request.POST['nama_petugas']
        split= petugas.split('-')
        new_ktp = split[0]
        startDate = request.POST['startDate']
        endDate = request.POST['endDate']
        stasiun = request.POST['stasiun']
        old_ktp = request.POST['ktp']

        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')

        cursor.execute("select start_datetime from penugasan where ktp='"+old_ktp+"';")
        old_date = cursor.fetchone()
        old_date = old_date[0]

        cursor.execute("select id_stasiun from penugasan where ktp ='"+old_ktp+"';")
        old_stasiun = cursor.fetchone()
        old_stasiun = old_stasiun[0]

        cursor.execute("select id_stasiun from stasiun where nama='"+stasiun+"';")
        new_stasiunId = cursor.fetchone()
        new_stasiunId = new_stasiunId[0]
        cursor.execute("update penugasan set ktp = '"+new_ktp+"',start_datetime = '"+str(startDate)+"', id_stasiun = '"+new_stasiunId+"', end_datetime = '"+str(endDate)+"' where ktp ='"+old_ktp+"'and start_datetime = '"+str(old_date)+"'and id_stasiun = '"+old_stasiun+"';")

        messages.error(request, "success")
        return HttpResponseRedirect('/bikeshare/daftarPenugasan')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/bikeshare/daftarPenugasan')

@csrf_exempt
def detail_daftarPenugasan(request):
    if(request.method=="POST"):
        response={}
        no_ktp = request.POST['ktp']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select * from penugasan where ktp='"+no_ktp+"';")
        daftar_penugasan = cursor.fetchall()
        cursor.execute("select nama from person where ktp='"+no_ktp+"';")
        name = cursor.fetchone()
        cursor.execute("select s.nama from stasiun as s, penugasan as p where p.ktp='"+no_ktp+"' and p.id_stasiun=s.id_stasiun")
        stasiun = cursor.fetchone()
        cursor.execute("select nama from stasiun")
        list_stasiun = cursor.fetchall()
        cursor.execute("select penugasan.ktp, person.nama from penugasan, person where person.ktp = penugasan.ktp;")
        nama_petugas = cursor.fetchall()
        response['nama_petugas'] = nama_petugas
        response['daftar_penugasan'] = daftar_penugasan[0]
        response['name'] = name[0]
        response['stasiun'] = stasiun
        response['list_stasiun'] = list_stasiun
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('bikeshare/daftarPenugasan/detail')


def createAcara(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("select nama from stasiun")

    result = cursor.fetchall()
    response['stasiun'] = result

    return render(request, 'createAcara.html', response)

def daftarAcara(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("select * from acara")

    result = cursor.fetchall()
    response['acara'] = result

    return render(request, 'daftarAcara.html', response)

def newAcara(request):
    if (request.method == "POST"):
        judul = request.POST['judul']
        deskripsi = request.POST['deskripsi']
        biaya = request.POST['biaya']
        startDate = request.POST['startDate']
        endDate = request.POST['endDate']
        allStasiun = request.POST.getlist('stasiun')

        varBiaya = False
        if (biaya == 'Gratis'):
            varBiaya = True
        else:
            varBiaya = False

        cursor = connection.cursor()
        cursor.execute("select id_acara from acara order by cast (id_acara as int) desc;")
        result = cursor.fetchall()

        id_acara = int(hasil[0]) +1
        cursor.execute("INSERT INTO acara(id_acara,judul,tgl_mulai,is_free,tgl_akhir,deskripsi) values("+"'"+str(id_acara)+"','" +str(judul)+"','"  + startDate +"'," + str(varBiaya) +",'" + endDate + "','" + str(deskripsi) +"');")

        for i in allStasiun:
            cursor.execute("select id_stasiun from stasiun  where nama ='"+i+"' ")
            id_stasiun = cursor.fetchone()
            cursor.execute("INSERT INTO acara_stasiun(id_stasiun,id_acara) values("+"'"+str(id_stasiun[0])+"','"  + str(id_acara) +"');")

        messages.error(request, "success")
        return HttpResponseRedirect('/bikeshare/daftarAcara')

    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/bikeshare/createAcara')

@csrf_exempt
def deleteAcara(request):
    if(request.method == "POST"):
        id_acara = request.POST['id']
        cursor = connection.cursor()
        cursor.execute("delete from acara where id_acara='"+id_acara+"';")
        messages.error(request, 'success')
        return HttpResponseRedirect('/bikeshare/daftarAcara')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/bikeshare/daftarAcara')

@csrf_exempt
def updateAcara(request):
    if(request.method=="POST"):
        id_acara = request.POST['id_acara']
        judul = request.POST['judul']
        deskripsi = request.POST['deskripsi']
        fee= request.POST['fee']
        mulai=request.POST['mulai']
        selesai=request.POST['selesai']
        list_stasiun=request.POST.getlist('stasiun[]')
        biaya = False
        if(fee == 'gratis'):
            biaya = True
        else:
            biaya = False

        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("update acara set judul='"+judul+"', is_free='"+str(biaya)+"', tgl_mulai='"+mulai+"', tgl_akhir='"+selesai+"', deskripsi='"+deskripsi+"' where id_acara = '"+id_acara+"';")
        cursor.execute("select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='"+id_acara+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = cursor.fetchall()
        for i in range(0,len(list_stasiun)):

            id_stasiun_lama=''
            id_stasiun=''
            for i in nama_stasiun:
                print(i[0])
                cursor.execute("select id_stasiun from stasiun  where nama ='"+i[0]+"' ")
                id_stasiun_lama = cursor.fetchone()
                id_stasiun_lama=id_stasiun_lama[0]

            for j in list_stasiun:
                cursor.execute("select id_stasiun from stasiun  where nama ='"+j+"' ")
                id_stasiun = cursor.fetchone()
                id_stasiun=id_stasiun[0]


            cursor.execute("update acara_stasiun set id_stasiun='"+id_stasiun+"' where id_acara='"+id_acara+"' and id_stasiun='"+id_stasiun_lama+"';")

        messages.error(request, "success")
        return HttpResponseRedirect('/bikeshare/daftarAcara')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/bikeshare/daftarAcara')

@csrf_exempt
def detailAcara(request):
    if(request.method=="POST"):
        response={}
        acara= request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_station,public')
        cursor.execute("select * from acara where id_acara='"+acara+"';")
        id_acara = cursor.fetchone()
        cursor.execute("select nama from stasiun")
        stasiun = cursor.fetchall()
        cursor.execute("select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='"+acara+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = cursor.fetchall()
        response['stasiun'] = stasiun
        response['acara'] = id_acara
        response['nama_stasiun'] = nama_stasiun
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/bikeshare/daftarAcara')


def createPeminjaman(request):
    return render(request, 'createPeminjaman.html')

def daftarPeminjaman(request):
    return render(request, 'daftarPeminjaman.html')

def formStasiun(request):
    return render(request, 'formStasiun.html')

def daftarStasiun(request):
    return render(request, 'daftarStasiun.html')

def formSepeda(request):
    return render(request, 'formSepeda.html')

def daftarSepeda(request):
    return render(request, 'daftarSepeda.html')
