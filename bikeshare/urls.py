from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'bikeshare'
urlpatterns = [
    path('formPenugasan/', views.formPenugasan, name='formPenugasan'),
    path('formPenugasan/create', views.createPenugasan, name='formPenugasan'),
    path('daftarPenugasan/delete', views.delete_daftarPenugasan, name='deletePenugasan'),
    path('daftarPenugasan/', views.daftarPenugasan, name='daftarPenugasan'),
    path('daftarPenugasan/detail', views.detail_daftarPenugasan, name='daftarPenugasan'),
    path('createAcara/', views.createAcara, name='createAcara'),
    path('createAcara/new', views.newAcara, name='newcreateAcara'),
    path('daftarAcara/', views.daftarAcara, name='daftarAcara'),
    path('daftarAcara/delete', views.deleteAcara, name='daftarAcara'),
    path('daftarAcara/detail', views.detailAcara, name='daftarAcara'),
    path('daftarAcara/update', views.updateAcara, name='daftarAcara'),
    path('createPeminjaman/', views.createPeminjaman, name='createPeminjaman'),
    path('daftarPeminjaman/', views.daftarPeminjaman, name='daftarPeminjaman'),
    path('formStasiun/', views.formStasiun, name='formStasiun'),
    path('daftarStasiun/', views.daftarStasiun, name='daftarStasiun'),
    path('formSepeda/', views.formSepeda, name='formSepeda'),
    path('daftarSepeda/', views.daftarSepeda, name='daftarSepeda')

]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
