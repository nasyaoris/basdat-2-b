from django.urls import path
from django.conf.urls import url
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name = 'transaksi'
urlpatterns = [
    path('riwayat/', views.riwayat, name='riwayat'),
    path('topup/', views.topup, name='topup'),
    path('topupAttempt/', views.topupAttempt, name='topupAttempt'),
    path('laporan/', views.laporan, name='laporan')
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
