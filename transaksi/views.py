from __future__ import unicode_literals

from django.shortcuts import render
from django.db import connection
from django.contrib import messages
import http.client
import json
from django.views.decorators.csrf import csrf_exempt
import datetime
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
# -*- coding: utf-8 -*-




def riwayat(request):
    cursor = connection.cursor()
    response={}
    ktp = request.session['ktp']
    cursor.execute("select t.no_kartu_anggota, t.date_time,t.jenis,t.nominal from anggota as a, transaksi as t where a.ktp='"+ktp+"' and t.no_kartu_anggota = a.no_kartu;")
    value = cursor.fetchall()
    response['transaksi'] = value

    return render (request, 'riwayat.html',response)

    

def topup(request):
    return render(request, 'topup.html')

def laporan(request):
    cursor = connection.cursor()
    response={}
    cursor.execute("select n.nama, l.id_laporan,l.no_kartu_anggota, l.datetime_pinjam, l.status, p.denda from person as n, laporan as l,  anggota as a, peminjaman as p where  n.ktp = a.ktp and a.no_kartu = l.no_kartu_anggota and l.no_kartu_anggota = p.no_kartu_anggota and l.datetime_pinjam= p.datetime_pinjam and l.nomor_sepeda = p.nomor_sepeda;")
    value = cursor.fetchall()
    response['laporan'] = value
    print(response)
    return render (request, 'laporan.html', response)

@csrf_exempt
def topupAttempt(request):  
    cursor = connection.cursor()
    if(request.method=="POST"):
        saldoInput = request.POST['topup']    
        ktp = request.session['ktp']
        cursor.execute("SELECT no_kartu FROM ANGGOTA WHERE ktp = '"+ktp+"' ")
        data = cursor.fetchone() 
        no_kartu = data[0]
        date_time = datetime.datetime.now()
        time = date_time.strftime('%m/%d/%Y %H:%M:%S')
        jenis = 'TopUp'
        value = "'"+no_kartu+"','"+time+"','"+jenis+"','"+saldoInput + "'"
        cursor.execute("INSERT INTO TRANSAKSI (no_kartu_anggota,date_time,jenis,nominal) VALUES ("+value+")")
        messages.error(request, "success")
        return HttpResponseRedirect('/transaksi/topup/')
        
    else:
        messages.error(request, "transaksi gagal")
        return HttpResponseRedirect('/transaksi/topup/')

 


# Create your views here.
